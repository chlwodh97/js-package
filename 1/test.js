function solution(numer1, denom1, numer2, denom2) {
    let arr = [];

    let MA = numer1 * denom2;
    let MB = denom1 * denom2;
    let MC = numer2 * denom1;

    let plus = MA + MC;

    let MM = getGCD(plus, MB);

    arr.push(plus / MM)
    arr.push(MB / MM)

    return arr;
}

function getGCD(num1, num2) {
    let gcd = 1;

    for (let i = 2; i <= Math.min(num1, num2); i++) {
        if (num1 % i === 0 && num2 % i === 0) gcd = i;
    }
    return gcd;
}

// -------------------------------------------------------------------
function solution(numer1, denom1, numer2, denom2) {
    let MA = numer1*denom2;
    let MB = denom1*denom2;
    let MC = numer2*denom1;

    let plus = MA + MC;

    let getGCD = (num1, num2) => {
        let gcd = 1;

        for(let i=2; i<=Math.min(num1, num2); i++){
            if(num1 % i === 0 && num2 % i === 0){
                gcd = i;
            }
        }
        return gcd;
    }

    let MM =  getGCD(plus,MB);
    let arr = [];
    let MAA = plus / MM;
    let MBB = MB / MM;

    arr.push(MAA)
    arr.push(MBB)

    return arr;
}