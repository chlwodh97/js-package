
function camInit(stream) {
    let cameraView : HTMLElement = document.getElementById("cameraview");
    cameraView.srcObject = stream;
    cameraView.onplay;
    //cameraView.play()
}



function camInitFailed(error) {
    console.log("get camera permission failed : ", error);
}

// Main init
function mainInit() {
    // Check navigator media device available
    if (!navigator.mediaDevices || !navigator.mediaDevices.getUserMedia) {
        // Navigator mediaDevices not supported
        alert("Media Device not supported");
        // 메서드를 끝내겠다.
        return;
    }
    navigator.mediaDevices.getUserMedia({video: true})
        .then(camInit)
        .catch(camInitFailed);
}